import multiprocessing
import paho.mqtt.publish as publish
import time as t
import requests
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from subprocess import check_output


def messages():

        try:
            MQTT_SERVER = "192.168.0.210"
            MQTT_PATH = "upload_1"

            # The callback for when the client receives a CONNACK response from the server.
            def on_connect(client, userdata, flags, rc):
                print("Connected with result code " + str(rc))

                # Subscribing in on_connect() means that if we lose the connection and
                # reconnect then subscriptions will be renewed.
                client.subscribe(MQTT_PATH)

                # The callback for when a PUBLISH message is received from the server.

            def on_message(client, userdata, msg):
                # 23/18Z3.15/3.25
                mensaje = str(msg.payload)
                print (mensaje)

                f = open("array.txt", "a+")
                f.write(mensaje + "\n")
                f.close()



            client = mqtt.Client()
            client.on_connect = on_connect
            client.on_message = on_message

            client.connect(MQTT_SERVER, 1883, 60)

            # Blocking call that processes network traffic, dispatches callbacks and
            # handles reconnecting.
            # Other loop*() functions are available that give a threaded interface and a
            # manual interface.
            client.loop_forever()

        except:
            pass

def upload():

    array = []

    while True:
        try:
            flag = 0
            with open("array.txt", "r") as f:
                for line in f:
                    line = json.loads(line[:-1])
                    array.append(line)

                flag = 1


            if flag == 1:
                print("removing")
                os.remove("array.txt")

            print("array...........")
            print (array)
            t.sleep(2)

        except:
            print("error Archivo")

        try:
            data = json.dumps(array)
            print(data)
            res = requests.post("http://192.168.0.2:4000/b/", data=data)
            print(res.status_code)

            if res.status_code == 200:

                array = []
                print(array)

            t.sleep(10)

        except:
            print("Error request, json")
            t.sleep(10)


if __name__ == "__main__":

        p1 = multiprocessing.Process(target=messages)
        p2 = multiprocessing.Process(target=upload)


        p1.start()
        p2.start()
